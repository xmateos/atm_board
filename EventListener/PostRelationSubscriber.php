<?php

namespace ATM\BoardBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class PostRelationSubscriber implements EventSubscriber{

    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata
        );
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();


        if ($metadata->getName() == 'ATM\BoardBundle\Entity\Post') {

            $metadata->mapManyToOne(array(
                'targetEntity' => $this->config['post'],
                'fieldName' => 'post_to_link',
                'joinColumns' => array(
                    array(
                        'name' => 'post_id',
                        'referencedColumnName' => 'id',
                        'onDelete' => 'SET NULL'
                    )
                )
            ));
        }


    }
}
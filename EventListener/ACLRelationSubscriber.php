<?php

namespace ATM\BoardBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class ACLRelationSubscriber implements EventSubscriber{

    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata
        );
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();


        if ($metadata->getName() == 'ATM\BoardBundle\Entity\ACL') {

            $metadata->mapManyToOne(array(
                'targetEntity' => $this->config['user'],
                'fieldName' => 'user',
                'joinColumns' => array(
                    array(
                        'name' => 'user_id',
                        'referencedColumnName' => 'id'
                    )
                )
            ));
        }


    }
}
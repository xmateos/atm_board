<?php

namespace ATM\BoardBundle\Extension;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use \DateTime;

class BoardExtension extends AbstractExtension
{
    private $em;
    private $tokenStorage;
    private $config;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage,$atm_board_config)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->config = $atm_board_config;
    }

    public function getFunctions()
    {
        return array(
            new TwigFunction('AtmBoardUserHasAccess', array($this, 'userHasAccess')),
            new TwigFunction('AtmBoardMaxNumberPostsPerDay', array($this, 'maxNumberPostsPerDay'))
        );
    }

    public function userHasAccess($userId,$boardId){
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('acl')
            ->from('ATMBoardBundle:ACL','acl')
            ->join('acl.user','u','WITH',$qb->expr()->eq('u.id',$userId))
            ->join('acl.board','b','WITH',$qb->expr()->eq('b.id',$boardId));

        $acl = $qb->getQuery()->getOneOrNullResult();

        return is_null($acl) ? false : true;
    }

    public function maxNumberPostsPerDay(){
        $user = $this->tokenStorage->getToken()->getUser();
        $maxPostsPerDay = $this->config['max_posts_per_day'];
        $currentDate = new DateTime();

        $qbIds = $this->em->createQueryBuilder();
        $qbIds
            ->select('COUNT(p.id) as total_posts')
            ->from('ATMBoardBundle:Post','p')
            ->join('p.board','b')
            ->join('b.user','u','WITH',$qbIds->expr()->eq('u.id',$user->getId()))
            ->where(
                $qbIds->expr()->andX(
                    $qbIds->expr()->gte('p.creation_date',$qbIds->expr()->literal($currentDate->format('Y-m-d').' 00:00:00')),
                    $qbIds->expr()->lte('p.creation_date',$qbIds->expr()->literal($currentDate->format('Y-m-d').' 23:59:59'))
                )
            );

        $totalPosts = $qbIds->getQuery()->getArrayResult();

        return $totalPosts[0]['total_posts'] < $maxPostsPerDay ? true : false;
    }
}
<?php

namespace ATM\BoardBundle\Repository;

use Doctrine\ORM\EntityRepository;
use XLabs\ResultCacheBundle\Repository\BaseRepository;
use ATM\BoardBundle\Entity\Post;

class PostRepository extends BaseRepository
{
    public function _getItemById($post_id)
    {
        $em = $this->getEntityManager();
        $cache = $em->getConfiguration()->getResultCacheImpl();
        $qb = $em->createQueryBuilder();

        $qb_total_comments = $em->createQueryBuilder();
        $qb_total_comments
            ->select($qb_total_comments->expr()->count('DISTINCT c.id'))
            ->from('MMCoreBundle:Comment\Comment', 'c')
            ->join('c.thread', 'c_t', 'WITH', $qb_total_comments->expr()->eq('c_t.id', $qb_total_comments->expr()->literal('boardpost_'.$post_id)))
        ;

        $post = $qb
            ->select('post')
            ->addSelect('board')
            ->addSelect('user')
            ->addSelect('images')
            ->addSelect('video')
            ->addSelect('pl')
            //->addSelect('(SELECT DISTINCT c.id FROM ATMCommentBundle:Comment c WHERE c.thread_id = e.id) AS comments');
            ->addSelect('('.
                $qb_total_comments->getQuery()->getDQL()
                .') AS total_comments')
            ->from(Post::class, 'post')
            ->join('post.board', 'board')
            ->join('board.user','user')
            ->leftJoin('post.images', 'images')
            ->leftJoin('post.video', 'video')
            ->leftJoin('post.post_to_link','pl')
            ->where($qb->expr()->eq('post.id', $post_id))
            ->getQuery()
        ;

        $cache_config = method_exists($cache, 'getConfig') ? $cache->getConfig() : false; // XLabsResultCache config
        if(!$cache_config || !isset($cache_config['custom_cache_enabled']) || !$cache_config['custom_cache_enabled'])
        {
            $post
                ->useQueryCache(true)
                ->setResultCacheLifetime(Post::RESULT_CACHE_ITEM_TTL)
                ->setResultCacheId(Post::RESULT_CACHE_ITEM_PREFIX.$post_id)
            ;
        } else {
            $post->useQueryCache(true);
        }

        $post = $post->getArrayResult();
        if($post)
        {
            $post = $post[0];
            $post[0]['total_comments'] = $post['total_comments'];
            unset($post['total_comments']);
            return $post[0];
        } else {
            $this->clearItemCache($post_id);
            return false;
        }
    }

    public function clearItemCache($post_id)
    {
        $em = $this->getEntityManager();
        $em->getConfiguration()->getResultCacheImpl()->delete(Post::RESULT_CACHE_ITEM_PREFIX.$post_id);
    }
}
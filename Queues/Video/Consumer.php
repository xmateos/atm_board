<?php

namespace ATM\BoardBundle\Queues\Video;

use XLabs\RabbitMQBundle\RabbitMQ\Consumer as Parent_Consumer;
use Doctrine\ORM\EntityManagerInterface;
use Qencode\Exceptions\QencodeApiException;
use Qencode\Exceptions\QencodeException;
use Qencode\QencodeApiClient;
use \Imagick;
use ATM\BoardBundle\Entity\Post as BoardPost;

class Consumer extends Parent_Consumer
{
    // set your custom consumer command name
    protected static $consumer = 'atm_board_encode_video:execute';
    private $em;
    private $rootDir;
    private $config;

    public function __construct(EntityManagerInterface $em, $kernel_rootdir,$atm_board_config)
    {
        parent::__construct();
        $this->em = $em;
        $this->rootDir = $kernel_rootdir;
        $this->config = $atm_board_config;
    }

    // following function is required as it is
    protected function configure()
    {
        $this
            ->setName(self::$consumer)
        ;
    }

    // following function is required as it is
    public function getQueueName()
    {
        return Producer::getQueueName();
    }

    public function callback($msg)
    {
        //return true;
        try{
            $body = json_decode($msg->body,true);

            $videoFilename = $body['videoFilename'];
            $postId = $body['post_id'];
            $user_id = $body['user_id'];
            $user = $this->em->getRepository($this->config['user'])->findOneById($user_id);

            $userFolderName = $user->getUsernameCanonical();
            $userDir = $this->rootDir.'/../web/'.$this->config['media_folder'].'/'.$userFolderName.'/videos';

            $fileName = md5(uniqid());
            $fileTokens = explode('.',$videoFilename);
            $destination_filename = $fileName.'.'.$fileTokens[1];
            $web_folder = $this->rootDir.'/../web/';

            if(strpos($videoFilename,' ') !== FALSE)
            {
                $oldVideoFilename = $videoFilename;
                $videoFilename = str_replace(' ','',$videoFilename);
                rename($userDir.'/output/'.$oldVideoFilename,$userDir.'/output/'.$videoFilename);
            }
            $source_media_path = $userDir.'/output/'.$videoFilename;
            $source_media_path = str_replace($web_folder, '', $source_media_path);

            $qencode_formats = array();
            $query = array();

            // Video
            $qencode_format_params = array(
                'output' => 'mp4',
                'size' => '1920x1080',
                'profile' => 'main',
                'tag' => json_encode(array(
                    'destination_filename' => $destination_filename
                )),
                'bitrate' => 4800,
                'video_codec' => 'libx264',
                'resize_mode' => 'crop' // should add black bars to vertical videos
            );
            if($this->config['qencode']['watermark'])
            {
                // documentation says "it will be good idea to have different size logo images for output streams of different resolutions."
                $watermark_file_and_position = $this->getWatermarkFileAndPosition($this->config, 1920, 1080);
                $qencode_format_params['logo'] = array(
                    'source' => $watermark_file_and_position['url'],
                    'x' => $watermark_file_and_position['x'],
                    'y' => $watermark_file_and_position['y'],
                    'opacity' => 1,
                );
            }
            $qencode_formats[] = $qencode_format_params;

            // Thumbnail
            $qencode_thumbnail_format_params = array(
                'output' => 'thumbnail',
                'time' => '0.10', // 10% of video duration
                'image_format' => 'jpg'
                //width' => '320',
                //height' => '700',
            );
            $qencode_formats[] = $qencode_thumbnail_format_params;

            $query['query']['format'] = $qencode_formats;

            $query['query']['source'] = $this->config['qencode']['source_url'].'/'.$source_media_path;
            $query['query']['callback_url'] = $this->config['qencode']['callback_url'];
            $query['query']['upscale'] = 1;
            $query['query']['encoder_version'] = 2; // fix watermark issues at QEncode end
            $payload = array(
                'id' => $postId,
                'original_file' => $source_media_path
            );
            //dump($query, $payload); die;
            //mail('xavi.mateos@manicamedia.com', 'QEncode call', json_encode($query).'<br /><br />'.json_encode($payload));
            //if(!$this->localDevelopment->isLocalhost())
            //{
                $q = new QencodeApiClient($this->config['qencode']['apiKey']);
                $task = $q->createTask();
                $task->startCustom(json_encode($query), json_encode($payload));
                //mail('xavi.mateos@manicamedia.com', 'QEncode call sent!!!', 'OH NO!');
            //}
            return true;
        }catch(\Exception $e){
            $post = $this->em->getRepository(BoardPost::class)->find($postId);
            if($post)
            {
                $this->em->remove($post);
            }
            return false;
        }

        /*$q = new QencodeApiClient($apiKey);
        try {

            $task = $q->createTask();  // /v1/create_task
            dump("Created task: ".$task->getTaskToken());
            $task->start_time = '';
            $task->duration = '';

            $task->start($transcodingProfileId, $videoUrl, $transferMethodId);

            do {
                sleep(5);
                $response = $task->getStatus();
                if (is_array($response) and array_key_exists('percent', $response)) {
                    dump("Completed: {$response['percent']}%");
                }
            } while ($response['status'] != 'completed');

            foreach ($response['videos'] as $video) {
                $encodedVideoPath = $this->rootDir.'/../web/uploads/qencode/1080/'.basename($video['storage']['path']);
                copy($encodedVideoPath,$userDir.'/'.$destVideoName);
                unlink($encodedVideoPath);

                $imageName = str_replace('.mp4','.jpg',basename($video['storage']['path']));
                $imagePath = $this->rootDir.'/../web/uploads/qencode/image/'.$imageName;
                copy($imagePath,$userDir.'/'.$imageName);
                unlink($imagePath);

                $post = $this->em->getRepository('ATMBoardBundle:Post')->findOneById($postId);
                if($post){
                    $video = $post->getVideo();
                    $video->setVideo($this->config['media_folder'].'/'.$userFolderName.'/videos/'.$destVideoName);
                    $video->setThumbnail($this->config['media_folder'].'/'.$userFolderName.'/videos/'.$imageName);
                    $video->setIsEncoded(true);

                    $this->em->persist($video);
                    $this->em->flush();
                }
            }

        }catch(QencodeClientException $e){
            dump('Qencode Client Exception: ' . $e->getCode() . ' ' . $e->getMessage());
            $post = $this->em->getRepository('ATMBoardBundle:Post')->findOneById($postId);
            if($post){
                $this->em->remove($post);
            }

        }catch(QencodeApiException $e){
            // API response status code was not successful
            dump('Qencode API Exception: ' . $e->getCode() . ' ' . $e->getMessage());
            $post = $this->em->getRepository('ATMBoardBundle:Post')->findOneById($postId);
            if($post){
                $this->em->remove($post);
            }

        }catch(QencodeException $e){
            dump('Qencode Exception: ' . $e->getMessage());
            dump($q->getLastResponseRaw());
            $post = $this->em->getRepository('ATMBoardBundle:Post')->findOneById($postId);
            if($post){
                $this->em->remove($post);
            }
        }catch(\Exception $e){
            $post = $this->em->getRepository('ATMBoardBundle:Post')->findOneById($postId);
            if($post){
                $this->em->remove($post);
            }
        }*/
    }

    private function getWatermarkFileAndPosition($config, $video_width, $video_height)
    {
        $watermark = $config['qencode']['watermark'];
        $web_folder = $this->rootDir.'/../web/';
        $watermark_file = $web_folder.$watermark['path'];
        $pathinfo_watermark = pathinfo($watermark_file);
        $watermark_file_for_resolution = $pathinfo_watermark['dirname'].'/'.$pathinfo_watermark['filename'].'_'.$video_width.'x'.$video_height.'.'.$pathinfo_watermark['extension'];
        if(file_exists($watermark_file_for_resolution))
        {
            $im_watermark = new Imagick($watermark_file_for_resolution);
        } else {
            $im_watermark = $watermark_file instanceof Imagick ? $watermark_file : new Imagick($watermark_file);
            $width_W = $im_watermark->getImageWidth();
            $height_W = $im_watermark->getImageHeight();

            // Save watermark for that resolution
            if($watermark['resize_factor'])
            {
                $desired_watermark_size = 1/$watermark['resize_factor'];
                $W_w = ceil($video_width * $desired_watermark_size);
                $H_w = ($height_W * $W_w) / $width_W;
                $im_watermark->scaleImage($W_w, $H_w);
            }

            $im_watermark->writeImage($watermark_file_for_resolution);
            $im_watermark->clear();
            $im_watermark->destroy();
            $im_watermark = new Imagick($watermark_file_for_resolution);
        }

        // Positioning
        switch($watermark['positionX'])
        {
            case 'left':
                $x = ($watermark['marginX'] / 100) * $video_width;
                break;
            case 'center':
                $x = ($video_width - $im_watermark->getImageWidth()) / 2;
                break;
            case 'right':
            default:
                $x = ($video_width - $im_watermark->getImageWidth()) - (($watermark['marginX'] / 100) * $video_width);
                break;
        }
        switch($watermark['positionY'])
        {
            case 'top':
                $y = ($watermark['marginY'] / 100) * $video_height;
                break;
            case 'center':
                $y = ($video_height - $im_watermark->getImageHeight()) / 2;
                break;
            case 'bottom':
            default:
                $y = ($video_height - $im_watermark->getImageHeight()) - (($watermark['marginY'] / 100) * $video_height);
                break;
        }

        return array(
            'url' => $config['qencode']['source_url'].'/'.str_replace($web_folder, '', $watermark_file_for_resolution),
            'x' => (int)$x,
            'y' => (int)$y,
        );
    }
}


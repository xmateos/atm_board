<?php

namespace ATM\BoardBundle\Queues\DownloadOutput;

use XLabs\RabbitMQBundle\RabbitMQ\Consumer as Parent_Consumer;
use ATM\BoardBundle\Entity\Post as BoardPost;
use MM\UserBundle\Services\FolderManager;
use MM\CoreBundle\Helpers\FileSystem;
use Symfony\Component\Console\Helper\ProgressBar;

class Consumer extends Parent_Consumer
{
    protected static $consumer = 'atm_board:qencode:download';
    private $web_folder;

    protected function configure()
    {
        $this
            ->setName(self::$consumer)
        ;
    }

    public function getQueueName()
    {
        return Producer::getQueueName();
    }

    public function callback($msg)
    {
        $container = $this->getApplication()->getKernel()->getContainer();
        $em = $container->get('doctrine.orm.default_entity_manager');
        $this->web_folder = $container->get('kernel')->getRootDir().'/../web/';
        $folder_manager = $container->get(FolderManager::class);
        $config = $container->getParameter('atm_board_config');

        $em->clear();

        $msg = json_decode($msg->body, true);
        $payload = $msg['payload'];
        $videos = $msg['videos'];
        $images = $msg['images'];

        $board_post_id = $payload['id'];
        $original_file = $payload['original_file'];

        $post = $em->getRepository(BoardPost::class)->find($board_post_id);
        if(!$post)
        {
            return true;
        }

        $user = $post->getBoard()->getUser();
        $userFolderName = $user->getUsernameCanonical();
        $userDir = $config['media_folder'].'/'.$userFolderName.'/videos';

        foreach($videos as $output)
        {
            if(!isset($output['error']) || !$output['error'])
            {
                $user_tag = json_decode($output['user_tag'], true);
                $destination_filename = $user_tag['destination_filename'];

                FileSystem::createFolder($this->web_folder.$userDir, 0777);
                $destination_file = $userDir.'/'.$destination_filename;
                if($this->download($output['url'], $this->web_folder.$destination_file))
                {
                    @chmod($this->web_folder.$destination_file, 0777);

                    $video = $post->getVideo();
                    $video->setVideo($destination_file);
                    //$video->setThumbnail($this->config['media_folder'].'/'.$userFolderName.'/videos/'.$imageName);
                    $video->setIsEncoded(true);

                    $em->persist($video);
                    $em->flush();

                    /*$event = new DownloadFinished(array(
                        'post' => $post,
                        'qencode_output' => $msg,
                        'timestamp' => time()
                    ));
                    $container->get('event_dispatcher')->dispatch($event::NAME, $event);*/
                } else {
                    /*$event = new DownloadFinished(array(
                        'post' => $post,
                        'qencode_output' => $msg,
                        'timestamp' => time()
                    ));
                    $container->get('event_dispatcher')->dispatch($event::NAME, $event);*/
                }
            } else {
                // can be a connection error at qencode end; might be able to retry the job
                // delete board post and notify user
                //$em->remove($post);
                //$em->flush();
            }
            @unlink($this->web_folder.$original_file);
        }

        foreach($images as $output)

        {
            if(!isset($output['error']) || !$output['error'])
            {
                $destination_filename = basename($output['storage']['path']);
                $destination_file = $userDir.'/'.$destination_filename;

                if($this->download($output['url'], $this->web_folder.$destination_file))
                {
                    $video = $post->getVideo();
                    $video->setThumbnail($destination_file);

                    /*$event = new DownloadFinished(array(
                        'post' => $post,
                        'qencode_output' => $msg,
                        'timestamp' => time()
                    ));
                    $container->get('event_dispatcher')->dispatch($event::NAME, $event);*/
                }
            }
        }

        $em->persist($video);
        $em->flush();
    }

    private function download($asset, $destination)
    {
        $destination_fp = fopen($destination, 'a');
        $ch = curl_init($asset);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($ch, CURLOPT_NOPROGRESS, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_FILE, $destination_fp);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0');
        $CURLOPT_HTTPHEADER = array(
            "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
            "Cache-Control: max-age=0",
            "Connection: keep-alive",
            "Keep-Alive: 300",
            "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7",
            "Accept-Language: en-us,en;q=0.5",
            "Pragma: "
        );

        // Progress
        $download_progress = array(
            'resume_from_size' => 0,
            'download_start_time' => 0,
            'progress' => 0
        );
        $progress_bar = new ProgressBar($this->output, 100);
        $progress_bar->start();
        //$progress_bar->setFormat('[<info>%bar%</info>] %current%/%max% <comment>%message%</comment> %percent:3s%%');
        $progress_bar->setFormat('[<info>%bar%</info>] %percent:3s%% <comment>%message%</comment>');
        $progress_bar->setMessage($asset);
        curl_setopt($ch, CURLOPT_PROGRESSFUNCTION, function($res, $download_size, $downloaded_size, $upload_size, $uploaded_size) use (&$download_progress, &$progress_bar){
            $this->progressCallback($res, $download_size, $downloaded_size, $upload_size, $uploaded_size, $download_progress, $progress_bar);
        });

        // Resume if file exists
        if(file_exists($destination))
        {
            $file_already_downloaded = $this->curl_get_file_size($asset) == filesize($destination);
            if($file_already_downloaded)
            {
                fclose($destination_fp);
                $progress_bar->finish();
                $this->output->writeln('');
                return $destination;
            }
            $download_progress['resume_from_size'] = filesize($destination);
            $CURLOPT_HTTPHEADER[] = 'Range: bytes='.$download_progress['resume_from_size'].'-';
            curl_setopt($ch, CURLOPT_RANGE, $download_progress['resume_from_size'].'-');
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $CURLOPT_HTTPHEADER);
        curl_exec($ch);
        fclose($destination_fp);

        if(curl_error($ch))
        {
            //dump(curl_error($ch));
            return false;
        }
        $progress_bar->finish();
        $this->output->writeln('');
        curl_close($ch);
        return true;
    }

    private function progressCallback($res, $download_size, $downloaded_size, $upload_size, $uploaded_size, &$download_progress, &$progress_bar)
    {
        if($download_size > 0 && $downloaded_size > 0)
        {
            if($download_progress['resume_from_size'] > 0)
            {
                $resumed_download_size = $download_progress['resume_from_size'] + $download_size;
                $resumed_downloaded_size = $download_progress['resume_from_size'] + $downloaded_size;
                $progress = round($resumed_downloaded_size / $resumed_download_size * 100);
            } else {
                $progress = round($downloaded_size / $download_size * 100);
            }

            $now = microtime(true);
            $download_progress['download_start_time'] = $download_progress['download_start_time'] > 0 ? $download_progress['download_start_time'] : $now;
            if($progress > $download_progress['progress'])
            {
                $download_progress['progress'] = $progress;

                $transfer_time = $now - $download_progress['download_start_time'];
                $transfer_percentage = $downloaded_size / $download_size;
                $estimated_tranfer_time = $transfer_time / $transfer_percentage;
                $estimated_time_remaining = round($estimated_tranfer_time - $transfer_time);

                $estimated_time_remaining = sprintf('%02d:%02d:%02d', ($estimated_time_remaining/3600),($estimated_time_remaining/60%60), $estimated_time_remaining%60);

                $currentSpeed = $transfer_time > 0 ? ($downloaded_size / $transfer_time) : 0;

                //$progress_bar->setMessage();
                $progress_bar->setProgress((int)$download_progress['progress']);
                $progress_details = array(
                    'percentage_progress' => $download_progress['progress'],
                    'ETA' => $estimated_time_remaining,
                    'speed' => FileSystem::formatBytes($currentSpeed).'/s',
                );
            }
        }
    }

    private function curl_get_file_size($asset)
    {
        // Assume failure.
        $result = -1;

        $curl = curl_init($asset);

        // Issue a HEAD request and follow any redirects.
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0');

        $data = curl_exec($curl);
        curl_close($curl);

        if($data)
        {
            $content_length = "unknown";
            $status = "unknown";
            if(preg_match("/^HTTP\/1\.[01] (\d\d\d)/", $data, $matches))
            {
                $status = (int)$matches[1];
            }
            if(preg_match("/Content-Length: (\d+)/", $data, $matches))
            {
                $content_length = (int)$matches[1];
            }
            // http://en.wikipedia.org/wiki/List_of_HTTP_status_codes
            if($status == 200 || ($status > 300 && $status <= 308))
            {
                $result = $content_length;
            }
        }
        return $result;
    }
}
<?php

namespace ATM\BoardBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class PostDeleted extends Event
{

    const NAME = 'atm_board_post_deleted.event';

    private $postOwnerEmail;
    private $postId;
    private $postDescription;
    private $reason;

    public function __construct($postOwnerEmail,$postId,$postDescription = null,$reason = null)
    {
        $this->postOwnerEmail = $postOwnerEmail;
        $this->postId = $postId;
        $this->postDescription = $postDescription;
        $this->reason = $reason;
    }

    public function getPostOwnerEmail()
    {
        return $this->postOwnerEmail;
    }

    public function setPostOwnerEmail($postOwnerEmail)
    {
        $this->postOwnerEmail = $postOwnerEmail;
    }

    public function getPostId()
    {
        return $this->postId;
    }
    public function setPostId($postId)
    {
        $this->postId = $postId;
    }
    public function getPostDescription()
    {
        return $this->postDescription;
    }
    public function setPostDescription($postDescription)
    {
        $this->postDescription = $postDescription;
    }

    public function getReason()
    {
        return $this->reason;
    }

    public function setReason($reason)
    {
        $this->reason = $reason;
    }
}


<?php

namespace ATM\BoardBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder->root('atm_board')
            ->children()
                ->scalarNode('user')->isRequired()->end()
                ->scalarNode('post')->isRequired()->end()
                ->scalarNode('media_folder')->isRequired()->end()
                ->scalarNode('image_width')->isRequired()->end()
                ->scalarNode('image_height')->isRequired()->end()
                ->scalarNode('watermark_image_small')->isRequired()->end()
                ->scalarNode('watermark_image_medium')->isRequired()->end()
                ->scalarNode('watermark_image_big')->isRequired()->end()
                //->scalarNode('encoder_api_key')->isRequired()->end()
                //->scalarNode('encoder_profile_id')->isRequired()->end()
                //->scalarNode('encoder_transfer_method_id')->isRequired()->end()
                ->scalarNode('not_access_redirect_route')->defaultValue('atm_board_see_request_access_link')->end()
                ->scalarNode('after_request_access_redirect_route')->defaultValue(null)->end()
                ->scalarNode('added_to_acl_redirect_route')->defaultValue(null)->end()
                ->scalarNode('removed_to_acl_redirect_route')->defaultValue(null)->end()
                ->scalarNode('max_posts_per_day')->defaultValue(null)->end()
                ->arrayNode('qencode')->isRequired()
                    ->children()
                        ->scalarNode('apiKey')->isRequired()->end()
                        ->arrayNode('watermark')->isRequired()
                            ->children()
                                ->scalarNode('path')->end()
                                ->scalarNode('resize_factor')->end()
                                ->scalarNode('positionX')->end()
                                ->scalarNode('positionY')->end()
                                ->scalarNode('marginX')->end()
                                ->scalarNode('marginY')->end()
                            ->end()
                        ->end()
                        ->scalarNode('source_url')->isRequired()->end()
                        ->scalarNode('callback_url')->isRequired()->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}

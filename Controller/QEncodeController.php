<?php

namespace ATM\BoardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use ATM\BoardBundle\Queues\DownloadOutput\Producer as DownloadOutputProducer;

class QEncodeController extends Controller
{
    /**
     * @Route("/atm_board_posts/qencode/callback", name="atm_board_qencode_callback")
     */
    public function qencodeCallbackAction(Request $request, EntityManagerInterface $em)
    {
        //$request = $this->get('request_stack')->getCurrentRequest();
        $params = $request->request->all();
        $status = json_decode($params['status'], true);
        $payload = isset($params['payload']) ? json_decode($params['payload'], true) : array();
        if($status['status'] == 'completed')
        {
            if($payload)
            {
                if($status['videos'])
                {
                    $this->get(DownloadOutputProducer::class)->process(array(
                        'images' => $status['images'],
                        'videos' => $status['videos'],
                        'payload' => $payload
                    ));
                } else {
                    if($status['error'])
                    {
                        /*$entityType = $payload['entity'];
                        $entityId = $payload['id'];
                        $e = $em->getRepository($entityType)->find($entityId);
                        if($e)
                        {
                            $e->setStatus($e::STATUS_ERROR_ENCODING);
                            $em->persist($e);
                            $em->flush();
                        }*/
                    }
                }
            }
        }
        //mail('xavi.mateos@manicamedia.com', 'QEncode Callback', json_encode($params));
        return new Response('Ok');
    }
}
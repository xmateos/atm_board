<?php

namespace ATM\BoardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;
use ATM\BoardBundle\Services\ImageManager;
use ATM\BoardBundle\Services\Resumable;
use ATM\BoardBundle\Form\BoardType;
use ATM\BoardBundle\Helpers\Image as ImageHelper;
use ATM\BoardBundle\Entity\ACL;
use ATM\BoardBundle\Entity\Board;
use ATM\BoardBundle\Entity\Post;
use ATM\BoardBundle\Entity\Image;
use ATM\BoardBundle\Entity\Video;
use ATM\BoardBundle\Event\RequestAccessToBoard;
use ATM\BoardBundle\Event\AddedTodACL;
use ATM\BoardBundle\Event\RemovedFromACL;
use ATM\BoardBundle\Event\PostDeleted;
use ATM\BoardBundle\Services\SearchPosts;
use ATM\BoardBundle\Queues\Video\Producer as VideoProducer;
use MM\SearchBundle\Services\SearchPost;
use MM\MediaBundle\Entity\Post as SitePost;

class BoardController extends Controller
{
    /**
     * @Route("/timeline/{usernameCanonical}/{mediaType}/{page}", name="atm_board_timeline", defaults={"page":1,"mediaType":"all"}, options={"expose"=true})
     */
    public function timelineAction($usernameCanonical,$mediaType,$page)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $board = $em->getRepository('ATMBoardBundle:Board')->getBoard($usernameCanonical);
        $config = $this->getParameter('atm_board_config');

        if(!is_null($board)){
            $hasAccess = true;
            if($board['user']['id'] != $user->getId()){
                if($board['visibility'] != Board::VISIBILITY_PUBLIC){
                    $hasAccess = $em->getRepository('ATMBoardBundle:ACL')->userHasAccess($board['id'],$user->getId());
                }
                $userProfile = $em->getRepository('MMUserBundle:User')->findOneBy(array(
                    'id' => $board['user']['id']
                ));
            }else{
                $userProfile = $user;
            }

            $boardPostsDates = $em->getRepository('ATMBoardBundle:Board')->getBoardPostsDates($board['id']);
            $disabledDates = array();
            foreach($boardPostsDates as $postDate){
                if($postDate['totalPosts'] >= $config['max_posts_per_day']){
                    $creationDate = \DateTime::createFromFormat('Y-m-d',$postDate['creationDate']);
                    $disabledDates[] = $creationDate->format('d m Y');
                }
            }

            $mmadmin = $this->get('xlabs_mm_admin');
            return $this->render('ATMBoardBundle:Board:board.html.twig',array(
                'board' => $board,
                'page' => $page,
                'mediaType' => $mediaType,
                'profile' =>$userProfile,
                'hasAccess' => $mmadmin->isMMAdmin() ? true : $hasAccess,
                'config' => $config,
                'disabledDates' => $disabledDates
            ));
        }else{
            if($usernameCanonical != $user->getUsernameCanonical()){
                $this->get('session')->getFlashBag()->set('atm_board_not_exist','The board you are trying to see doesn\'t exists');

                return $this->redirect($this->get('router')->generate($config['not_access_redirect_route']));
            }

            return $this->redirect($this->get('router')->generate('atm_board_create'));
        }
    }

    /**
     * @Route("/see/request/access/link/{boardId}/{userId}", name="atm_board_see_request_access_link")
     */
    public function seeRequestAccessLinkAction($boardId,$userId){
        $em = $this->getDoctrine()->getManager();

        $board = $em->getRepository('ATMBoardBundle:Board')->findOneById($boardId);
        return $this->render('ATMBoardBundle:Board:request_board_access.html.twig',array(
            'boardId' => $boardId,
            'userId'=>$userId,
            'board' => $board
        ));
    }

    /**
     * @Route("/create/board", name="atm_board_create")
     */
    public function createBoardAction(){
        $board = new Board();
        $form = $this->createForm(BoardType::class,$board);
        $request = $this->get('request_stack')->getCurrentRequest();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $config = $this->getParameter('atm_board_config');
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $mediaDir = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'];
            if(!is_dir($mediaDir)){
                mkdir($mediaDir);
            }

            $userFolderName = $user->getUsernameCanonical();
            $userFolder = $mediaDir.'/'.$userFolderName;
            if(!is_dir($userFolder)){
                mkdir($userFolder);
            }

            $image = $request->files->get('flImage');
            $extension = $image->guessExtension();
            $filename = md5(uniqid()).'.'.$extension;
            $image->move($userFolder,$filename);
            $this->get(ImageManager::class)->cropImage($userFolder.'/'.$filename,$filename,$userFolder,false);

            $board->setThumbnail($config['media_folder'].'/'.$userFolderName.'/'.$filename);
            $board->setUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($board);
            $em->flush();

            return $this->redirect($this->get('router')->generate('atm_board_timeline',array('usernameCanonical'=> $user->getUsernameCanonical())));
        }

        return $this->render('ATMBoardBundle:Board:create_board.html.twig',array(
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/edit/board/{boardId}", name="atm_board_edit")
     */
    public function editBoardAction($boardId){
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $board = $em->getRepository('ATMBoardBundle:Board')->findOneById($boardId);

        if($user->getId() != $board->getUser()->getId()){
            return $this->redirect($this->get('router')->generate('atm_board_timeline',array('usernameCanonical'=> $user->getUsernameCanonical())));
        }

        $form = $this->createForm(BoardType::class,$board);
        $request = $this->get('request_stack')->getCurrentRequest();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $image = $request->files->get('flImage');
            if(!is_null($image)){
                $config = $this->getParameter('atm_board_config');
                $imagePath = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'].'/'.$board->getUser()->getUsernameCanonical();
                $imageName = md5(uniqid()).'.'.$image->guessExtension();
                unlink($this->get('kernel')->getRootDir().'/../web/'.$board->getThumbnail());
                $image->move($imagePath,$imageName);
                $this->get(ImageManager::class)->cropImage($imagePath.'/'.$imageName,$imageName,$imagePath,false);
                $board->setThumbnail($config['media_folder'].'/'.$board->getUser()->getUsernameCanonical().'/'.$imageName);
            }

            $em->persist($board);
            $em->flush();

            return $this->redirect($this->get('router')->generate('atm_board_timeline',array('usernameCanonical'=>$this->get('security.token_storage')->getToken()->getUser()->getUsernameCanonical())));
        }

        return $this->render('ATMBoardBundle:Board:edit_board.html.twig',array(
            'form' => $form->createView(),
            'board' => $board
        ));
    }

    /**
     * @Route("/post/image", name="atm_board_upload_image", options={"expose"=true})
     */
    public function uploadImageAction(){
        $request = $this->get('request_stack')->getCurrentRequest();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if($request->getMethod() == 'POST'){
            $em = $this->getDoctrine()->getManager();
            $config = $this->getParameter('atm_board_config');
            $boardId = $request->get('boardId');

            $board = $em->getRepository('ATMBoardBundle:Board')->findOneById($boardId);

            if($board->getUser()->getId() == $user->getId()){
                $post = new Post();
                $post->setBoard($board);

                $description = $request->get('txDescription');
                if(!is_null($description)){
                    $post->setDescription($description);
                }

                $creationDate = $request->get('creation_date');
                if(!empty($creationDate)){
                    $creationDate = \DateTime::createFromFormat('d-m-Y H:i',$creationDate);
                    $post->setCreationDate($creationDate);
                }else{
                    $post->setCreationDate(new \DateTime());
                }

                $postLinkedId = $request->get('post_link_id');
                if(!is_null($postLinkedId)){
                    $postToLink = $em->getRepository($config['post'])->findOneById($postLinkedId);
                    $post->setPostToLink($postToLink);
                }

                $em->persist($post);

                $mediaDir = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'];
                if(!is_dir($mediaDir)){
                    mkdir($mediaDir);
                }

                $userFolderName = $user->getUsernameCanonical();
                $userFolder = $mediaDir.'/'.$userFolderName;
                if(!is_dir($userFolder)){
                    mkdir($userFolder);
                }

                $userFolder .= '/images';
                if(!is_dir($userFolder)){
                    mkdir($userFolder);
                }


                $images = $request->files->get('flImage');
                $mainImage = $request->get('main_image');

                $count = 1;
                foreach($images as $image){
                    $extension = $image->guessExtension();
                    $filename = md5(uniqid()).'.'.$extension;
                    $image->move($userFolder,$filename);

                    $image = new Image();
                    if($extension != 'gif'){

                        ImageHelper::correctImageOrientation($userFolder.'/'.$filename);
                        ImageHelper::removeExif($userFolder.'/'.$filename);

                        //Crop and Watermark thumbnail image
                        $thumbnail = md5(uniqid()).'.'.$extension;
                        $this->get(ImageManager::class)->cropImage($userFolder.'/'.$filename,$thumbnail,$userFolder);

                        //Watermark original image
                        $this->get(ImageManager::class)->watermarkImage($userFolder.'/'.$filename,$userFolder.'/'.$filename);

                        $image->setImage($config['media_folder'].'/'.$userFolderName.'/images/'.$filename);
                        $image->setThumbnail($config['media_folder'].'/'.$userFolderName.'/images/'.$thumbnail);
                    }else{
                        $thumbnail = ImageHelper::getFirstFrameImageFromGif($config['media_folder'].'/'.$userFolderName.'/images/'.$filename,$config['media_folder'].'/'.$userFolderName.'/images/');
                        $this->get(ImageManager::class)->watermarkImage($userFolder.'/'.$thumbnail,$userFolder.'/'.$thumbnail);
                        $image->setImage($config['media_folder'].'/'.$userFolderName.'/images/'.$filename);
                        $image->setThumbnail($config['media_folder'].'/'.$userFolderName.'/images/'.$thumbnail);
                    }
                    if($count == $mainImage){
                        $image->setIsMain(true);
                    }

                    $count++;
                    $image->setPost($post);
                    $em->persist($image);
                }

                $em->flush();
            }else{
                $this->get('session')->getFlashBag()->set('atm_board_not_allowed','You are not allowed.');
            }

        }
        return $this->redirect($this->get('router')->generate('atm_board_timeline',array('usernameCanonical'=> $user->getUsernameCanonical())));
    }

    /**
     * @Route("/post/video", name="atm_board_upload_video", options={"expose"=true})
     */
    public function uploadVideoAction(){
        $request = $this->get('request_stack')->getCurrentRequest();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if($request->getMethod() == 'POST'){
            $em = $this->getDoctrine()->getManager();
            $boardId = $request->get('boardId');
            $config = $this->getParameter('atm_board_config');
            $board = $em->getRepository('ATMBoardBundle:Board')->findOneById($boardId);

            if($board->getUser()->getId() == $user->getId()){
                $videoFilename = $request->get('videoFilename');
                $description = $request->get('txDescription');

                $post = new Post();
                $post->setDescription($description);
                $post->setBoard($board);

                $creationDate = $request->get('creation_date');
                if(!empty($creationDate)){
                    $creationDate = \DateTime::createFromFormat('d-m-Y H:i',$creationDate);
                    $post->setCreationDate($creationDate);
                }else{
                    $post->setCreationDate(new \DateTime());
                }

                $postLinkedId = $request->get('post_link_id');
                if(!is_null($postLinkedId)){
                    $postToLink = $em->getRepository($config['post'])->findOneById($postLinkedId);
                    $post->setPostToLink($postToLink);
                }

                $video = new Video();
                $video->setPost($post);
                $video->setVideo(null);
                $video->setThumbnail(null);

                $em->persist($post);
                $em->persist($video);
                $em->flush();

                $this->get(VideoProducer::class)->process(array(
                    'videoFilename' => $videoFilename,
                    'user_id' => $user->getId(),
                    'post_id' => $post->getId()
                ));


                $this->get('session')->getFlashBag()->set('atm_board_video_encoding','Your video is being encoded, it will appear on your board once the encoding process is finished.');

            }else{
                $this->get('session')->getFlashBag()->set('atm_board_not_allowed','You are not allowed.');
            }
        }

        return $this->redirect($this->get('router')->generate('atm_board_timeline',array('usernameCanonical'=> $user->getUsernameCanonical())));
    }

    /**
     * @Route("/video/permissions", name="atm_board_video_permissions", options={"expose"=true})
     */
    public function setUploadedFilePermissionsAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $config = $this->getParameter('atm_board_config');
        $mediaDir = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'];
        if(!is_dir($mediaDir)){
            mkdir($mediaDir);
        }
        $userTempFolder = $mediaDir.'/'.$user->getUsernameCanonical();
        if(!is_dir($userTempFolder)){
            mkdir($userTempFolder);
        }

        $userTempFolder = $mediaDir.'/'.$user->getUsernameCanonical().'/videos';
        if(!is_dir($userTempFolder)){
            mkdir($userTempFolder);
        }

        $request = $this->get('request_stack')->getCurrentRequest();

        try {
            chmod($userTempFolder.'/output'.'/'.$request->request->get('file'), 0777);
            $response = $request->request->get('file');
        } catch (\Exception $e) {
            $response = 'ko';
        }
        return new Response($response);
    }

    /**
     * @Route("/video/plugin", name="atm_board_video_chunk_upload", options={"expose"=true})
     */
    public function uploadAction()
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $config = $this->getParameter('atm_board_config');
        $mediaDir = $this->get('kernel')->getRootDir().'/../web/'.$config['media_folder'];
        if(!is_dir($mediaDir)){
            mkdir($mediaDir);
        }

        $userFolderName = $user->getUsernameCanonical();
        $userFolder = $mediaDir.'/'.$userFolderName;
        if(!is_dir($userFolder)){
            mkdir($userFolder);
        }

        $userFolder.= '/videos';
        if(!is_dir($userFolder)){
            mkdir($userFolder);
            mkdir($userFolder.'/temp');
            mkdir($userFolder.'/output');
        }

        $upload_folder = $userFolder;
        $resumable = $this->get(Resumable::class);
        $resumable->tempFolder = $upload_folder.'/temp';
        $resumable->uploadFolder = $upload_folder.'/output';
        $resumable->process();
        return new Response($upload_folder);
    }

    /**
     * @Route("/create/text/post", name="atm_board_create_text_post", options={"expose"=true})
     */
    public function createTextPostAction(){
        $request = $this->get('request_stack')->getCurrentRequest();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $boardId = $request->get('boardId');

        if($request->getMethod() == 'POST'){
            $em = $this->getDoctrine()->getManager();

            $board = $em->getRepository('ATMBoardBundle:Board')->findOneById($boardId);

            if($board->getUser()->getId() == $user->getId()){
                $description = $request->get('txDescription');
                $post = new Post();
                $post->setBoard($board);
                $post->setDescription($description);

                $em->persist($post);
                $em->flush();
            }else{
                $this->get('session')->getFlashBag()->set('atm_board_not_allowed','You are not allowed.');
            }
        }

        return $this->redirect($this->get('router')->generate('atm_board_timeline',array('usernameCanonical'=> $user->getUsernameCanonical())));
    }

    public function searchAction($params){

        $posts = $this->get(SearchPosts::class)->search($params);

        return $this->render('ATMBoardBundle:Board:posts.html.twig',array(
            'posts' => $posts['results'],
            'pagination' => $posts['pagination']
        ));
    }

    /**
     * @Route("/delete/post/{postId}", name="atm_board_delete_post", options={"expose"=true})
     */
    public function deletePostAction($postId){
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $mmadmin = $this->get('xlabs_mm_admin');

        $post = $em->getRepository('ATMBoardBundle:Post')->findOneById($postId);
        $boardOwner = $post->getBoard()->getUser();

        if($user->getId() == $post->getBoard()->getUser()->getId() || $mmadmin->isMMAdmin()){
            $boardOwnerEmail = $post->getBoard()->getUser()->getEmail();
            if($post->getImages()){
                foreach($post->getImages() as $image){
                    if(file_exists($this->get('kernel')->getRootDir().'/../web/'.$image->getImage())){
                        unlink($this->get('kernel')->getRootDir().'/../web/'.$image->getImage());
                        unlink($this->get('kernel')->getRootDir().'/../web/'.$image->getThumbnail());
                    }
                }

            }elseif($post->getVideo()){
                $video = $post->getVideo();
                unlink($this->get('kernel')->getRootDir().'/../web/'.$video->getVideo());
                unlink($this->get('kernel')->getRootDir().'/../web/'.$video->getThumbnail());
            }

            $postId = $post->getId();
            $postDescription = $post->getDescription();

            $request = $this->get('request_stack')->getCurrentRequest();
            $reason = $request->get('reason');

            $em->remove($post);
            $em->flush();

            $event = new PostDeleted($boardOwnerEmail,$postId,$postDescription,$reason);
            $this->get('event_dispatcher')->dispatch(PostDeleted::NAME, $event);

        }else{
            $this->get('session')->getFlashBag()->set('atm_board_not_allowed','You are not allowed to delete this post');
        }


        return $this->redirect($this->get('router')->generate('atm_board_timeline',array('usernameCanonical'=> $boardOwner->getUsernameCanonical())));
    }

    /**
     * @Route("/get/comments/{postId}", name="atm_board_get_comments", options={"expose"=true})
     */
    public function getCommentsAction($postId){

        return $this->render('ATMBoardBundle:Board:comments.html.twig',array(
            'postId' => $postId
        ));
    }


    /**
     * @Route("/request/access/board/{boardId}/{userId}", name="atm_board_request_access", options={"expose"=true})
     */
    public function requestAccessToBoardAction($boardId, $userId){

        $event = new RequestAccessToBoard($boardId, $userId);
        $this->get('event_dispatcher')->dispatch(RequestAccessToBoard::NAME, $event);

        $config = $this->getParameter('atm_board_config');

        if(is_null($config['after_request_access_redirect_route'])){
            return new Response('ok');
        }else{
            return $this->redirect($this->get('router')->generate($config['after_request_access_redirect_route']));
        }
    }

    /**
     * @Route("/add/user/acl/{boardId}/{userId}", name="atm_board_add_user_acl", options={"expose"=true})
     */
    public function addUserToAclAction($boardId, $userId){
        $config = $this->getParameter('atm_board_config');

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();
        $board = $em->getRepository('ATMBoardBundle:Board')->findOneById($boardId);

        if($user->getId() == $board->getUser()->getId()){

            if(!$em->getRepository('ATMBoardBundle:ACL')->userHasAccess($boardId, $userId)){
                $user = $em->getRepository($config['user'])->findOneById($userId);

                $acl = new ACL();
                $acl->setUser($user);
                $acl->setBoard($board);

                $em->persist($acl);
                $em->flush();
            }

            $event = new AddedTodACL($board, $user);
            $this->get('event_dispatcher')->dispatch(AddedTodACL::NAME, $event);

            if(is_null($config['added_to_acl_redirect_route'])){
                return new Response('ok');
            }else{
                return $this->redirect($this->get('route')->generate($config['added_to_acl_redirect_route']));
            }

        }else{
            return new Response('You are not allowed to give access to that board');
        }
    }

    /**
     * @Route("/remove/user/acl/{aclId}", name="atm_board_remove_user_acl", options={"expose"=true})
     */
    public function removeUserToAclAction($aclId){
        $config = $this->getParameter('atm_board_config');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();
        $acl = $em->getRepository('ATMBoardBundle:ACL')->findOneById($aclId);

        $board = $acl->getBoard();

        if($user->getId() == $board->getUser()->getId()){
            if(!is_null($acl)){
                $em->remove($acl);
                $em->flush();
            }

            $event = new RemovedFromACL($board->getId(), $board->getUser()->getId());
            $this->get('event_dispatcher')->dispatch(RemovedFromACL::NAME, $event);

            if(is_null($config['added_to_acl_redirect_route'])){
                return new Response('ok');
            }else{
                return $this->redirect($this->get('route')->generate($config['removed_to_acl_redirect_route']));
            }
        }else{
            return new Response('You are not allowed to remove access to that board');
        }
    }

    /**
     * @Route("/get/acl/users/{boardId}/{page}", name="atm_board_acl_users", options={"expose"=true}, defaults={"page":1})
     */
    public function getAclUsersAction($boardId,$page){
        $em = $this->getDoctrine()->getManager();

        $qbIds = $em->createQueryBuilder();

        $qbIds
            ->select('partial acl.{id}')
            ->from('ATMBoardBundle:ACL','acl')
            ->join('acl.board','board','WITH',$qbIds->expr()->eq('board.id',$boardId))
            ->orderBy('acl.creation_date','DESC');

        $arrIds = array_map(function($r){
            return $r['id'];
        },$qbIds->getQuery()->getArrayResult());

        $acls = array();
        $pagination = null;
        if(count($arrIds) > 0){
            $pagination = $this->get('knp_paginator')->paginate(
                $arrIds,
                $page,
                15
            );

            $ids = $pagination->getItems();


            $qb = $em->createQueryBuilder();
            $qb
                ->select('acl')
                ->addSelect('user')
                ->from('ATMBoardBundle:ACL','acl')
                ->leftJoin('acl.user','user')
                ->where($qb->expr()->in('acl.id',$ids))
                ->orderBy('acl.creation_date','DESC');

            $acls = $qb->getQuery()->getArrayResult();
        }

        return $this->render('ATMBoardBundle:Board:acl_user_results.html.twig',array(
            'acls' => $acls,
            'pagination' => $pagination,
            'page' => $page
        ));
    }

    /**
     * @Route("/get/post/popup/{postId}", name="atm_board_post_popup", options={"expose"=true})
     */
    public function postPopupAction($postId){
        $searchPost = $this->get(SearchPosts::class);

        $post = $searchPost->search(array('ids' => array($postId)));
        return $this->render('ATMBoardBundle:Board:item/popup.html.twig',array(
            'post' => $post['results']
        ));
    }

    /**
     * @Route("/post/details/{postId}", name="atm_board_post_details")
     */
    public function postDetailsAction($postId){
        $searchPost = $this->get(SearchPosts::class);

        $post = $searchPost->search(array('ids' => array($postId)));

        $postOwnerId = $post['results'][0]['board']['user']['id'];
        $em = $this->getDoctrine()->getManager();
        $userProfile = $em->getRepository('MMUserBundle:User')->findOneBy(array(
            'id' => $postOwnerId
        ));

        $showImageNumber = $this->get('request_stack')->getCurrentRequest()->get('i');

        if(is_null($showImageNumber) || (int)$showImageNumber > 4){
            $showImageNumber = 1;
        }

        return $this->render('ATMBoardBundle:Board:post_details.html.twig',array(
            'post' => $post['results'],
            'profile' => $userProfile,
            'showImageNumber' => $showImageNumber
        ));
    }

    /**
     * @Route("/create/image/post/{boardId}", name="atm_board_create_image_post")
     */
    public function createImagePostAction($boardId){
        $em = $this->getDoctrine()->getManager();
        $board = $em->getRepository('ATMBoardBundle:Board')->findOneById($boardId);
        $userProfile = $em->getRepository('MMUserBundle:User')->findOneBy(array(
            'id' => $board->getUser()->getId()
        ));
        $config = $this->getParameter('atm_board_config');

        $boardPostsDates = $em->getRepository('ATMBoardBundle:Board')->getBoardPostsDates($boardId);
        $disabledDates = array();
        foreach($boardPostsDates as $postDate){
            if($postDate['totalPosts'] >= $config['max_posts_per_day']){
                $creationDate = \DateTime::createFromFormat('Y-m-d',$postDate['creationDate']);
                $disabledDates[] = $creationDate->format('d m Y');
            }
        }

        return $this->render('ATMBoardBundle:Board:create_image_post.html.twig',array(
            'boardId' => $boardId,
            'profile' => $userProfile,
            'disabledDates' => $disabledDates
        ));
    }

    /**
     * @Route("/create/video/post/{boardId}", name="atm_board_create_video_post")
     */
    public function createVideoPostAction($boardId){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_board_config');

        $boardPostsDates = $em->getRepository('ATMBoardBundle:Board')->getBoardPostsDates($boardId);
        $disabledDates = array();
        foreach($boardPostsDates as $postDate) {
            if ($postDate['totalPosts'] >= $config['max_posts_per_day']) {
                $creationDate = \DateTime::createFromFormat('Y-m-d', $postDate['creationDate']);
                $disabledDates[] = $creationDate->format('d m Y');
            }
        }

        return $this->render('ATMBoardBundle:Board:create_video_post.html.twig',array(
            'boardId' => $boardId,
            'disabledDates' => $disabledDates
        ));
    }

    /**
     * @Route("/search/media/post/{title}", name="atm_board_search_media_post", options={"expose"=true})
     */
    public function searchMediaPostsAction($title){
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('partial p.{id,title,content_type}')
            ->from('MMMediaBundle:Post','p')
            ->join('p.owner','u','WITH',$qb->expr()->eq('u.id',$user->getId()))
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->like('p.title',$qb->expr()->literal('%'.$title.'%')),
                    $qb->expr()->eq('p.status',SitePost::STATUS_READY)
                )
            )
            ->orderBy('p.title','DESC');

        $posts = $qb->getQuery()->setMaxResults(10)->getArrayResult();

        return $this->render('ATMBoardBundle:Board:search_posts_results.html.twig',array(
            'posts' =>$posts
        ));
    }
}

<?php

namespace ATM\BoardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_board_video")
 */
class Video
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="video", type="string", length=255, nullable=true)
     */
    private $video;

    /**
     * @ORM\Column(name="thumbnail", type="string", length=255, nullable=true)
     */
    private $thumbnail;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    /**
     * @ORM\Column(name="is_encoded", type="boolean", nullable=false)
     */
    protected $isEncoded;

    /**
     * @ORM\OneToOne(targetEntity="Post", inversedBy="video")
     */
    protected $post;

    public function __construct(){
        $this->creation_date = new DateTime();
        $this->isEncoded = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function setVideo($video)
    {
        $this->video = $video;
    }

    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getPost()
    {
        return $this->post;
    }

    public function setPost($post)
    {
        $this->post = $post;
    }

    public function getIsEncoded()
    {
        return $this->isEncoded;
    }

    public function setIsEncoded($isEncoded)
    {
        $this->isEncoded = $isEncoded;
    }
}
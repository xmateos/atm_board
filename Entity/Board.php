<?php

namespace ATM\BoardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use XLabs\ResultCacheBundle\Annotations as XLabsResultCache;
use \DateTime;

/**
 * @ORM\Entity(repositoryClass="ATM\BoardBundle\Repository\BoardRepository")
 * @ORM\Table(name="atm_board_board")
 * @XLabsResultCache\Clear(onFlush={}, {
 *      @XLabsResultCache\Key(onFlush={"update", "delete"}, type="literal", method="getXLabsResultCacheKeyForItem"),
 *      @XLabsResultCache\Key(onFlush={"insert", "update", "delete"}, type="prefix", method="getXLabsResultCacheKeyForCollection")
 * })
 */
class Board{
    const VISIBILITY_PUBLIC = 0;
    const VISIBILITY_PRIVATE = 1;
    const VISIBILITY_SOLD = 2;

    const RESULT_CACHE_ITEM_PREFIX = 'atm_board_board';
    const RESULT_CACHE_ITEM_TTL = 36000000;
    const RESULT_CACHE_COLLECTION_PREFIX = 'atm_board_board';
    const RESULT_CACHE_COLLECTION_TTL = 36000000;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    protected $creation_date;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="board", cascade={"remove"})
     */
    protected $posts;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="thumbnail", type="string", length=255, nullable=false)
     */
    private $thumbnail;

    /**
     * @ORM\Column(name="description", type="text", nullable=true, options={"collation": "utf8mb4_unicode_ci"})
     */
    protected $description;

    /**
     * @ORM\Column(name="price",type="integer", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(name="visibility", type="smallint", nullable=false)
     */
    private $visibility;

    protected $user;

    public function __construct(){
        $this->creation_date = new DateTime();
        $this->posts = new ArrayCollection();
        $this->visibility = $this::VISIBILITY_PUBLIC;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getPosts()
    {
        return $this->posts;
    }

    public function addPost($post)
    {
        $this->posts->add($post);
    }

    public function removePost($post){
        $this->posts->removeElement($post);
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = strip_tags($description,'<span>');
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getVisibility()
    {
        return $this->visibility;
    }

    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;
    }

    public function getXLabsResultCacheKeyForItem()
    {
        return $this::RESULT_CACHE_ITEM_PREFIX.$this->getId();
    }

    public function getXLabsResultCacheKeyForCollection()
    {
        return $this::RESULT_CACHE_COLLECTION_PREFIX.$this->getId();
    }
}